﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public struct Difficulty
{
    public int level;
    public float spawnInterval;
    public float hungerGainPerSecond;
    public int scoreToNextLevel;
}

[System.Serializable]
[CreateAssetMenuAttribute(fileName="DifficultySettings", menuName = "DifficultySettings")]
public class DifficultySettings : ScriptableObject {
    public Difficulty[] settings;

    public Difficulty GetByScore(int score)
    {
        var lastLevel = settings[settings.Length - 1];
        if (score >= lastLevel.scoreToNextLevel) {
            return lastLevel;
        }

        foreach(var d in settings)
        {
            if (score < d.scoreToNextLevel)
                return d;
        }
        return settings[0]; //se não achar
    }
}
