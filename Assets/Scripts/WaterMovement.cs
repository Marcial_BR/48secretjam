﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WaterMovement : MonoBehaviour {
    [SerializeField] Vector3[] waterPositions;
    [SerializeField] float lerpDuration = 1f;

    int currentIndex = 0;
	// Use this for initialization
	void Start () {
        StartCoroutine(GoToNextPosition());
    }

    IEnumerator GoToNextPosition()
    {
        float time = 0;
        Vector3 oldPosition = transform.localPosition;
        Vector3 desiredPosition = waterPositions[currentIndex];
        while(time < lerpDuration)
        {
            time += Time.deltaTime;
            time = Mathf.Min(time, lerpDuration);
            float alpha = time / lerpDuration;
            transform.localPosition = Vector3.Lerp(oldPosition, desiredPosition, alpha);
            //Debug.Log(transform.position);
            yield return new WaitForSeconds(Time.deltaTime);
        }
        currentIndex++;
        if (currentIndex == waterPositions.Length)
            currentIndex = 0;
        StartCoroutine(GoToNextPosition());
    }
}
