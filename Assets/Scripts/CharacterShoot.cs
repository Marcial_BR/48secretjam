﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterShoot : MonoBehaviour {

    [SerializeField] GameObject projectileA;
    [SerializeField] GameObject projectileB;
    [SerializeField] Transform projectileReference;

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        if(Tutorial.instance.inTutorial == Tutorial.InTutorial.Tutorial)
            return;
            
        if (Input.GetMouseButtonDown(0))
        {
            SpawnProjectile(projectileA);
        }
        if (Input.GetMouseButtonDown(1))
        {
            SpawnProjectile(projectileB);
        }
	}
    void SpawnProjectile(GameObject projectile)
    {
        GameObject.Instantiate(projectile, projectileReference.position, projectileReference.rotation);
    }
}
