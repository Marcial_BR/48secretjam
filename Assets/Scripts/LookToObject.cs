﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LookToObject : MonoBehaviour {
    [SerializeField] Transform objectToLook;
    [SerializeField] string objectTag = "Player";

    private void Start()
    {
        if (!objectToLook)
        {
            objectToLook = GameObject.FindGameObjectWithTag(objectTag).transform;
        }
    }


    void Update () {
        if(objectToLook)
            transform.LookAt(objectToLook);	
	}
}
