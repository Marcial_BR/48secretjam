﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class Bumper : MonoBehaviour {

    [SerializeField] float torqueMax = 100;
    [SerializeField] float upForce = 10;
    [SerializeField] float timeToMove = 2f;
    [SerializeField] FoodType foodType;
    [SerializeField] float pointsToLoseWhenMissType = 20f;
    [SerializeField] GameObject popUp;
    [SerializeField] Color color;
    [SerializeField] GameObject[] birdsChildren;
    [SerializeField] Container currentContainer;
    [SerializeField] GameObject transformationParticles;
    [SerializeField] Vector3 particleScale;
    [SerializeField] GameObject particleFoodHit;

    int currentBirdIndex = 0;

    private void Update()
    {
        if (CanEvolve())
            EvolveBird();
    }
    private void OnTriggerEnter(Collider other)
    {
        Food food = other.transform.GetComponent<Food>();
        if (food && !food.hittedBumper)
        {
            food.hittedBumper = true;
            if (!currentContainer)
            {
                food.rb.AddTorque(GetRandomTorque(), ForceMode.Impulse);
                food.rb.velocity = Vector3.up * upForce;
            }
            else
            {
                StartCoroutine(MoveToContainer(food));
            }
            if (particleFoodHit)
                Instantiate(particleFoodHit, food.transform.position, food.transform.rotation);
            else
                Debug.LogError("Missing particle food hit");
        }
        else
        {
            //Debug.Log("Non Food  hit");
        }
    }
    Vector3 GetRandomTorque()
    {
        return new Vector3(Random.Range(-torqueMax, +torqueMax), Random.Range(-torqueMax, +torqueMax), Random.Range(-torqueMax, +torqueMax));
    }
    IEnumerator MoveToContainer(Food food)
    {
        float timeRemaining = timeToMove;
        food.rb.isKinematic = true;
        Vector3 oldPosition = food.transform.position;
        Vector3 oldScale = food.transform.localScale;
        while (timeRemaining > 0)
        {
            float alpha = 1 - (timeRemaining / timeToMove);
            Vector3 newPosition = currentContainer.mouthReference.position;
            float scaleFinal = .7f;
            food.transform.position = Vector3.Lerp(oldPosition, newPosition, alpha);
            food.transform.localScale = Vector3.Lerp(oldScale, new Vector3(scaleFinal, scaleFinal, scaleFinal), alpha);
            timeRemaining -= Time.deltaTime;
            yield return new WaitForSeconds(Time.deltaTime);
        }
        if(foodType == food.foodType || food.foodType == FoodType.pig)
        {
            CollectedFood(food);
        }
        else
        {
            SetPopUp("Blaaargh", food);
            WrongType(food);
        }
    }
    void WrongType(Food food)
    {
        food.Explode(pointsToLoseWhenMissType);
    }
    void CollectedFood(Food food)
    {       
        SetPopUp("+" + food.scorePerFood, food, true);
        food.Collect();
    }
    bool CanEvolve()
    {
        int currentLevel = GameManager.gManager.GetCurrentDifficulty().level;
        return (
            currentContainer &&
            currentLevel > currentContainer.levelToChangeContainer &&
            currentBirdIndex + 1 < birdsChildren.Length
            );
    }
    void EvolveBird()
    {
        HideAllBirds();
        currentBirdIndex++;
        currentContainer = birdsChildren[currentBirdIndex].GetComponent<Container>();
        currentContainer.gameObject.SetActive(true);
        if (transformationParticles)
        {
            var p = Instantiate(transformationParticles, transform.position, transform.rotation);
            p.transform.localScale = particleScale;
        }
    }
    void HideAllBirds()
    {
        foreach(var bird in birdsChildren)
        {
            bird.SetActive(false);
        }
    }

    private void SetPopUp (string _text, Food _food, bool _yes = false) {
        var txtpopUp = Instantiate(popUp, _food.transform.position, Quaternion.identity);
        txtpopUp.GetComponentInChildren<TextMeshPro>().text = _text;
        txtpopUp.GetComponentInChildren<TextMeshPro>().color = color;

        if(_yes)
            txtpopUp.GetComponent<AudioSource>().Play();
    }
}
