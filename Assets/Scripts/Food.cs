﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public enum FoodType
{
    red,
    green,
    blue,
    pig
}
public class Food : MonoBehaviour {
    public Rigidbody rb;
    public bool hittedBumper = false;
    public FoodType foodType;
    public float hungerLossWhenFeedBird = 10;
    public float scorePerFood = 10;

    [SerializeField] float pointsToLoseWhenHitTheGround = 10;
    [SerializeField] GameObject particlesExplode;
    [SerializeField] GameObject particlesCollect;
    [SerializeField] CharacterMovement player;

    void Start () {
        if (!rb)
        {
            rb = GetComponent<Rigidbody>();
        }
	}
	
	void Update () {
		
	}

    void HitByProjectile()
    {

    }
    private void OnCollisionEnter(Collision collision)
    {
        if (collision.transform.CompareTag("Floor"))
        {
            Explode();
        }
    }
    public void Explode(float pointsToLose = -1f)
    {
        if (pointsToLose == -1f)
        {
            pointsToLose = pointsToLoseWhenHitTheGround;
        }
        if (!player)
        {
            player = FindObjectOfType<CharacterMovement>();
        }
        player.camShake.Shake();
        SpawnParticlesAndDie(particlesExplode);
    }
    public void Collect()
    {
        SpawnParticlesAndDie(particlesCollect, 2f);
        GameManager.gManager.SetScore((int) Mathf.Round(scorePerFood));
        GameManager.gManager.IncrementHunger(hungerLossWhenFeedBird);
    }
    void SpawnParticlesAndDie(GameObject particles, float particleScale = 1f)
    {
        var spawned = Instantiate(particles, transform.position, Quaternion.identity);
        spawned.transform.localScale = new Vector3(particleScale, particleScale, particleScale);
        Object.Destroy(this.gameObject, .1f);
    }
}
