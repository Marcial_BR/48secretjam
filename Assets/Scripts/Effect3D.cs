﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Effect3D : MonoBehaviour {

	public float damping = 5;

	private void Start() {
		Destroy(gameObject, 1.5f);
	}

	void Update () {
		var lookPos = Camera.main.transform.position - transform.position;
		var rotation = Quaternion.LookRotation(-lookPos);
		transform.rotation = Quaternion.Slerp(transform.rotation, rotation, Time.deltaTime * damping);
	}
}
