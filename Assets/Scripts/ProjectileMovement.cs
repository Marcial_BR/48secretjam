﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProjectileMovement : MonoBehaviour {

    [SerializeField] float speed = 100f;
    [SerializeField] Rigidbody rb;
    [SerializeField] float impactForce = 300;
    [SerializeField] bool heavyHit = false;
    [SerializeField] GameObject particleImpact;
    [SerializeField] CharacterMovement player;
    [SerializeField] Transform target;
    [SerializeField] float timeToHitTarget = .4f;

    Vector3 initialPosition;

    bool didHit = false;
	// Use this for initialization
	void Start () {
        if (!rb)
            rb = GetComponent<Rigidbody>();

        SearchTarget();
        if (target)
        {
            rb.isKinematic = true;
            StartCoroutine(FollowTarget());
        }
        initialPosition = transform.position;
    }
    private void Update()
    {
        if (!player)
            player = FindObjectOfType<CharacterMovement>();
    }
    private void FixedUpdate()
    {
        if (!didHit && !rb.isKinematic)
        {
            rb.velocity = transform.forward * speed;
        }
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player") || other.CompareTag("Floor"))
            return;
        Food food = other.transform.GetComponent<Food>();
        if (food && !didHit)
        {
            HitFood(food);
        }
        if (particleImpact)
        {
            Instantiate(particleImpact, transform.position, transform.rotation);
        }
        didHit = true;
        Destroy(gameObject);
    }
    void HitFood(Food food)
    {
        Rigidbody foodRB = food.GetComponent<Rigidbody>();
        Vector3 forceDirection =  food.transform.position - initialPosition;
        if (heavyHit)
        {
            //forceDirection = (forceDirection.normalized + (Vector3.right)).normalized;
            forceDirection = forceDirection.normalized;
        }
        else
        {
            forceDirection = (forceDirection.normalized + Vector3.up/1.5f).normalized;
        }
        foodRB.AddForce(forceDirection * impactForce, ForceMode.Impulse);
        food.GetComponent<AudioSource>().Play();
    }
    void SearchTarget()
    {
        RaycastHit hit;
        Vector3 origin = transform.position;
        Vector3 direction = transform.forward;
        int foodLayer = LayerMask.GetMask("Food");
        bool hitted = Physics.SphereCast(origin, 4f, direction, out hit, 1000000, foodLayer);
        if (hit.collider)
        {
            target = hit.collider.transform;
        }
    }
    IEnumerator FollowTarget()
    {
        float totalTime = timeToHitTarget;
        while(totalTime > 0)
        {
            if (!target)
            {
                Destroy(gameObject);
                break;
            }
            totalTime -= Time.deltaTime;
            Vector3 oldPosition = transform.position;
            Vector3 newPosition = target.position;
            transform.position = Vector3.Lerp(oldPosition, newPosition, 1 - (totalTime / timeToHitTarget));
            transform.LookAt(target.transform);
            yield return new WaitForSeconds(Time.deltaTime);
        }
        HitFood(target.GetComponent<Food>());
    }
}
