﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraShake : MonoBehaviour {
    public float magnitude = 15f;
    public float speed = 4f;
    public float duration = .4f;

	public void Shake()
    {
        StartCoroutine(ShakeLoop());
    }
    IEnumerator ShakeLoop()
    {
        float timer = 0;
        Vector3 originalPosition = transform.localPosition;
        while(timer < duration)
        {
            timer += Time.deltaTime;
            timer = Mathf.Min(timer, duration);

            float x = Random.Range(-magnitude, magnitude);
            float y = Random.Range(-magnitude, magnitude);
            float z = transform.localPosition.z;

            Vector3 newPos = new Vector3(x, y, z);
            newPos = Vector3.Lerp(transform.localPosition, newPos, speed * Time.deltaTime);
            transform.localPosition = newPos;
            yield return new WaitForSeconds(Time.deltaTime);
        }
        transform.localPosition = originalPosition;
    }
}
