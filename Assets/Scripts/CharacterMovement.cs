﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterMovement : MonoBehaviour {

    
    Rigidbody rb;
    [SerializeField] float rotationSpeed = 1;
    [SerializeField] float movementAcceleration = 1;
    [SerializeField] float maxSpeed = 20f;
    [SerializeField] float decelerationSpeed = .5f;
    [SerializeField] float minPitch = -60f;
    [SerializeField] float maxPitch = 60f;

    public CameraShake camShake;


    // Use this for initialization
    void Start () {
        if (!rb)
            rb = GetComponent<Rigidbody>();
        if (!rb)
        {
            //Debug.Log("Esse objeto precisa de um rigidbody");
            Application.Quit();
        }
        Cursor.visible = false;
        Cursor.lockState = CursorLockMode.Locked;
    }

    // Update is called once per frame
    void Update () {
        if(Tutorial.instance.inTutorial == Tutorial.InTutorial.Tutorial)
            return;

        Rotate();
	}
    private void FixedUpdate()
    {
        if(Tutorial.instance.inTutorial == Tutorial.InTutorial.Tutorial)
            return;

        Move();
    }
    void Move()
    {
        var horizontal = Input.GetAxis("Horizontal");
        var vertical = Input.GetAxis("Vertical");
        var up = Input.GetAxis("MoveUp");

        ;
        Vector3 movementInput = (transform.forward * vertical) + (transform.up * up) + (transform.right * horizontal);
        if(movementInput.magnitude > 0)
        {
            rb.AddForce(movementInput * movementAcceleration); //mover
        }
        else
        {
            Decelerate();
            return;
        }
        if(rb.velocity.magnitude > maxSpeed)
        {
            //Limitar a velocidade
            rb.velocity = rb.velocity.normalized * maxSpeed;
        }
        //Debug.Log(rb.velocity.magnitude);
    }
    void Decelerate()
    {
        if(rb.velocity.magnitude > 0.1f)
        {
            rb.AddForce(rb.velocity.normalized * -1 * decelerationSpeed);
        }
    }
    void Rotate()
    {
        float x = Input.GetAxis("Mouse X") ;
        float y = Input.GetAxis("Mouse Y");
        Quaternion xQuaternion = Quaternion.AngleAxis(x * rotationSpeed * Time.deltaTime, Vector3.up);
        Quaternion yQuaternion = Quaternion.AngleAxis(y * rotationSpeed * Time.deltaTime, -Vector3.right);
        Quaternion newRotation = Camera.main.transform.localRotation * yQuaternion;
        //transform.Rotate(Vector3.up * Time.deltaTime * rotationSpeed * x);
        //transform.Rotate(-Vector3.right * Time.deltaTime * rotationSpeed * y);
        Camera.main.transform.localRotation = ClampRotationAroundXAxis(newRotation, minPitch, maxPitch);
        transform.localRotation = transform.localRotation * xQuaternion;

    }
    Quaternion ClampRotationAroundXAxis(Quaternion q, float minAngle, float maxAngle)
    {
        q.x /= q.w;
        q.y /= q.w;
        q.z /= q.w;
        q.w = 1.0f;

        float angleX = 2.0f * Mathf.Rad2Deg * Mathf.Atan(q.x);

        angleX = Mathf.Clamp(angleX, minAngle, maxAngle);
        q.x = Mathf.Tan(0.5f * Mathf.Deg2Rad * angleX);

        return q;
    }
}
