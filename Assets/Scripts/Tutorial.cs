﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class Tutorial : MonoBehaviour {
	public static Tutorial instance;

	public enum InTutorial { Tutorial, InGame}
	public InTutorial inTutorial;

	public GameObject panelTutorial;
	public TextMeshProUGUI txtTutorial;
	public GameObject[] imgTutorial;
	public string[] message;

	private int index = 0;

	private void Awake() {
		instance = this;
        StartCoroutine(ShowTutorialAfterDelay());
    }
    IEnumerator ShowTutorialAfterDelay()
    {
        yield return new WaitForSeconds(.5f);
        //PlayerPrefs.DeleteAll();
        if (!PlayerPrefs.HasKey("TUTORIAL"))
        {
            panelTutorial.SetActive(true);
            imgTutorial[0].SetActive(true);
            PlayerPrefs.SetInt("TUTORIAL", 1);
            Time.timeScale = 0;
        }
        else
        {
            GameManager.gManager.inTutorial = false;
            inTutorial = InTutorial.InGame;
            panelTutorial.SetActive(false);
            Time.timeScale = 1;
            GameManager.gManager.StartGame();
        }
    }



	void Start () {
		txtTutorial.text = message[0];
	}
	

	void Update () {
		if(inTutorial == InTutorial.InGame)
			return;

		if(index >= message.Length) {
			inTutorial = InTutorial.InGame;
			panelTutorial.SetActive(false);
		}

		if(Input.GetKeyDown(KeyCode.Space)) {
			index++;

			if(index < message.Length)
				txtTutorial.text = message[index];

			if(index < imgTutorial.Length) {
				foreach (var item in imgTutorial) {
					item.SetActive(false);
				}
				imgTutorial[index].SetActive(true);
			}
            else if(GameManager.gManager.inTutorial)
            {
                Time.timeScale = 1;
                GameManager.gManager.StartGame();
            }
        }
			
	
	}
	
	
}
