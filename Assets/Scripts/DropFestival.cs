﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DropFestival : MonoBehaviour {
	// Use this for initialization
	void Start () {
        GameManager.gManager.onGameStart += StartDrop;
	}
	
    IEnumerator RandomDrop()
    {
        while (true)
        {
            float interval = GameManager.gManager.GetCurrentDifficulty().spawnInterval;
            GameManager.gManager.SpawnFood(GetRandomPosition());
            yield return new WaitForSeconds(interval);
        }
    }
    Vector3 GetRandomPosition()
    {
        Vector3 rndPosWithin;
        rndPosWithin = new Vector3(Random.Range(-1f, 1f), Random.Range(-1f, 1f), Random.Range(-1f, 1f));
        rndPosWithin = transform.TransformPoint(rndPosWithin * .5f);
        return rndPosWithin;
    }

    void StartDrop()
    {
        StartCoroutine(RandomDrop());
    }
}
