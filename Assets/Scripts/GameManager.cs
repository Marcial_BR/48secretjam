﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour {
	public static GameManager gManager;

	[System.Serializable]
	public class Foods {
		public string foodType;
		public GameObject[] food;
	}

	[Header("Dificulty Settins")]
    public Difficulty currentLevel;
    public DifficultySettings difficultySettings;

	[Header("Other Settins")]
    public float minHunger = 0f;
    public float maxHunger = 100f;
    public float hunger = 100;
    [Range(0, 02)] public float hungerGainPerSecond = 5f;
    [Range(0, 10)] [SerializeField] float sliderSpeed = 5f;

	[Header("UI Settins")]
	public TextMeshProUGUI txtScore;
	public TextMeshProUGUI txtBest;
	public TextMeshProUGUI txtLevel;

	[Header("Hunger Settins")]
	[Range(0, 5)] public float hungerRate = 5;
	public Slider hungerSlider;

	[Header("GameOver Settins")]
	public GameObject gameOver;
	public GameObject player;

	[Header("Food Settins")]
	[Range(1, 10)]public float timeRate = 10;
	public GameObject pig;
	public Foods[] foods;

	private float rate;
	private int score;
	private int best;
	private int level = 1;
    public bool inTutorial = true;

    public event GameStarted onGameStart;

    public delegate void GameStarted();

    
	private void Awake() {
		Time.timeScale = 1;
		gManager = this;
		best = PlayerPrefs.GetInt("BEST");
		txtBest.text = "BEST: " + best;
	}

    private void Start()
    {
        if (!inTutorial)
        {
            StartGame();
        }
    }
    public void StartGame()
    {
        inTutorial = false;
        if (onGameStart != null)
        {
            onGameStart();
        }
    }
    private void Update () {
        if(Tutorial.instance.inTutorial == Tutorial.InTutorial.Tutorial)
            return;
        
        IncrementHunger(-GetCurrentDifficulty().hungerGainPerSecond * Time.deltaTime);
        SmoothUpdateSlider();
        if (Input.GetKeyDown(KeyCode.R))
        {
            Restart();
        }
        /* ------------------------------------ */
        //DEBUG - REMOVER  - IMPORTANTE
        //if (Input.GetKeyDown(KeyCode.X))
        //{
        //    SetScore(100);
        //}
        /* ------------------------------------ */
	}
    public Difficulty GetCurrentDifficulty()
    {
        return difficultySettings.GetByScore(score);
    }
	public void SpawnFood(Vector3 position) {
		var myRand = Random.Range(0, 100);
		var rtRand = new Vector3(Random.Range(0, 360), Random.Range(0, 360), Random.Range(0, 360));
		var myFood = (myRand <= 10) ? pig : foods[Random.Range(0, 3)].food[Random.Range(0, 3)];
		Instantiate(myFood, position, Quaternion.Euler(rtRand));
	}

	public void SetScore(int _score) {
		score += _score;
		txtScore.text = "SCORE: " + score;

		if(score > best) {
			PlayerPrefs.SetInt("BEST", score);
		    txtBest.text = "BEST: " + score;
        }

	}
    public void IncrementHunger(float value)
    {
        hunger += value;
        hunger = Mathf.Clamp(hunger, minHunger, maxHunger);
        if(hunger == minHunger)
        {
            GameOver();
        }
    }
    void SmoothUpdateSlider()
    {
        float newHunger = Mathf.Lerp(hungerSlider.value, hunger, Time.deltaTime * sliderSpeed);
        hungerSlider.value = newHunger;
    }

	
    public void GameOver() {
        Camera.main.transform.parent = this.transform;
		gameOver.SetActive(true);
		Time.timeScale = 0;
        Destroy(player.gameObject);
    }

    void Restart()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }
}
