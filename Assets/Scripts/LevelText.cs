﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class LevelText : MonoBehaviour {

    int level = 0;
    [SerializeField] TextMeshProUGUI textMesh;
	
	// Update is called once per frame
	void Update () {
        int currentLevel = GameManager.gManager.GetCurrentDifficulty().level;
        if(currentLevel != level)
        {
            level = currentLevel;
            textMesh.text = "Level: " + level;
        }
	}
}
