﻿Shader "Custom/Blink" {
	Properties{
		_MainTex("Base (RGB)", 2D) = "white" {}
		_Color("Color", Color) = (1,1,1,1)
		_Blink("Blink", Float) = 1
	}
		SubShader{
		Tags{ "RenderType" = "Opaque" }
		LOD 200

		CGPROGRAM
#pragma surface surf Lambert

		sampler2D _MainTex;
	float _Blink;

	struct Input {
		float2 uv_MainTex;
		float3 worldPos;
	};
	fixed4 _Color;
	float rand(float3 myVector) {
		return frac(sin(dot(myVector, float3(12.9898, 78.233, 45.5432))) * 43758.5453);
	}
	void surf(Input IN, inout SurfaceOutput o) {
		float3 vWPos = IN.worldPos;
		//float3 vTimeOffset=float3(1,1,1)*_Time[0];
		//vWPos+=vTimeOffset;

		float Rand1 = rand(round(vWPos));
		vWPos += float3(.5, .5, .5);

		Rand1 += rand(round(vWPos));
		vWPos -= float3(1, 1, 1);
		//Rand1+=rand(round(vWPos));
		Rand1 /= 2;
		half3 c = float3(Rand1, Rand1, Rand1) * _Color;
		if (_Blink == 1.0f)
			c *= (0.5f + abs(sin(_Time.w)));
		
		
		o.Albedo = c;
		o.Alpha = 1;
	}
	ENDCG
	}
		FallBack "Diffuse"
}